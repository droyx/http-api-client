from __future__ import absolute_import

import copy
import json
import logging
from functools import wraps
from inspect import signature

import six
from requests import Session
from requests.exceptions import RequestException


logger = logging.getLogger(__name__)


def endpoint(method_type, endpoint_path, attach_headers=True, parse_response_func=None):
    """
    Call method type request on endpoint name url
    :param method_type: request method type (GET, POST)
    :param endpoint_path: url or endpoint name in class endpoints map or endpoint path
    :param attach_headers: flag to decide if to request should be attached default headers
    :param parse_response_func: ffunction to parse response - default is use self.parse_response
    :return:
    f.e.
      @endpoint('POST', 'configuration', attach_headers=False)
      @endpoint('POST', 'configuration')
      @endpoint('POST', '/configuration/{session.credentials.client_id}')
      @endpoint('POST', 'https://www.ibm.com/api/v2/configuration/{session.credentials.client_id}')

     wraped functions:
       def func(function_attr, endpoint_param=None)
       def func(function_attr, endpoint_param='me')
       def func(function_attr)
       def func()
           return {"data":{}}

    _EXTRA: if endpoint is dynamic could pass None as `endpoint_path` and pass name parameter for endpoint function
      @endpoint('POST', None)
      def func(function_attr, endpoint_name=None)
        return {}
      then usage:
        api.func(top=1,  endpoint_name='https://ibm.com/api/contacts?next=token_a9da9wd')

     function_attr: function attributes used by function
     endpoint_param: parameters with name in endpoint path passed later to resolve path url

     function return request kwargs (data, headers,
    """
    def endpoint_wraper(func):
        @wraps(func)
        def wraper(self, *args, **kwargs):
            # ==== PREPARE ENDPOINT PATH ======
            endpoint_name = endpoint_path if endpoint_path else kwargs.get('endpoint_path', '')
            # --- add default parameters from endpoint function to called url kwargs --
            # --- allow to create endpoint method with format:
            # --- def endpoint_method(self, id='me') - 'me' will be passed to path '/some/path/{id}/'
            new_kwargs = copy.copy(kwargs)
            func_signature = signature(func)
            default_url_kwargs = {param.name: param.default
                                  for param in func_signature.parameters.values() if param.default is not param.empty}
            default_url_kwargs.update(new_kwargs)
            # --- final url for endpoint ---
            url = self.build_url(endpoint_name, **default_url_kwargs)
            # ===== CALLING ENDPOINT METHOD =====
            # --- method returns additional data for request like queryset params, json data, data, headers
            request_kwargs = func(self, *args, **new_kwargs)
            request_kwargs.update({'attach_headers': attach_headers})

            # ===== MAKING REQUEST ====
            result = self.request(method_type, url, **request_kwargs)

            # === PROCESSING RESPONSE ===
            parse_func = parse_response_func or self.parse_response
            return parse_func(result)
        return wraper
    return endpoint_wraper


class APIClient(Session):
    """
    Base on requests.Session API client base class
    Allow to create client for some external service API
    """
    host = ''  # ''https://api.watsonwork.ibm.com/api/' - must ends with '/'
    version = None
    version_attr = 'v'
    exchange_jwt = False

    def __init__(self, credentials, **kwargs):
        super(APIClient, self).__init__()
        self.credentials = credentials

        if self.exchange_jwt:
            self.credentials.jwt_token = self.get_app_jwt()

        for attr, val in six.iteritems(kwargs):
            setattr(self, attr, val)

    @property
    def endpoints(self):
        """
        Endpoints list
        Must be override
        Format:
           {'<endpoint_name>': <endpoint_path>}
        f.e.
          {
          'configuration': 'apps/{session.credentials.client_id}/configurationdata/{configuration_token}'
          }
          session - self
          configuration_token - param passed in @endpoit decored function
        >>>class API(APIClient):
        >>>   @endpoint('POST', 'configuration')
        >>>   def configure(self, extra_data, configuration_token=None):
        >>>       return {'data': extra_data}

        """
        return {}

    def get_version(self):
        """
        Prepare version param
        :return:
        """
        if self.version:
            return '{}{}/'.format(self.version_attr, self.version)
        return ''

    def build_url(self, path, **kwargs):
        """
        Builds absolute url path for endpoint
        :param path: full path with hos or endpoint path or endpoint name in self.endpoints
        :param kwargs:
        :return:
        """
        if path.startswith('http'):
            url = path
        elif path in self.endpoints:
            url = '{}{}{}'.format(self.host, self.get_version(), self.endpoints[path])
        else:
            url = '{}{}{}'.format(self.host, self.get_version(), path)
        return url.format(session=self, **kwargs)

    def get_default_headers(self):
        """
        Returns default headers for request
        Default:
          content-type: application/json,
          (optional) authorization: Bearer <token> | jwt: <jwt_token>
        :return: headers dict
        """
        headers = {'Content-Type': 'application/json'}
        if self.credentials:
            headers.update(self.credentials.default_authorization_headers)
        return headers

    @classmethod
    def handle_error(cls, response):
        """
        Check response code and body to raise request error if any
        :param response:
        :return:
        """
        if not 200 <= response.status_code < 300:
            raise RequestException(repr(response.content)[:250], response=response)
        if 'application/json' in response.headers.get('content-type', '').lower():
            data = json.loads(response.content)
            if 'error' in data or 'errors' in data:
                raise RequestException(repr(response.content)[:250], response=response)

    def request(self, method, url, *args, **kwargs):
        """
        Make request
        :param method:
        :param url:
        :param args:
        :param kwargs:
        :return:
        """
        logging.debug('Request {} {}'.format(method, url))
        attach_headers = kwargs.pop('attach_headers', True)
        self.headers = self.get_default_headers() if attach_headers else None

        response = super(APIClient, self).request(method, url, *args, **kwargs)

        # By explicitly closing the session, we avoid leaving sockets open which
        # can trigger a ResourceWarning in some cases, and look like a memory leak
        # in others.
        self.close()

        self.handle_error(response)
        return response

    @staticmethod
    def parse_response(response):
        """
        Parse response and return response body in different formats depends of response type
        Can be override and return response object or specific body format
        :param response: Response object
        :return: (str content|json|other format) default json if can or content
        """
        if 'application/json' in response.headers.get('content-type', '').lower():
            return json.loads(response.content)
        return response.content

    def get_jwt_headers(self):
        return {'jwt': self.credentials.jwt_token}

    def get_app_jwt(self):
        """
        get application token required for any future actions
        """
        # f.e. return self.jwt_endpoint()
        raise NotImplementedError()

    # =====================================================================
    # EXAMPLE
    # ====================================================================

    # @staticmethod # must be staticmethod if will be passed to function default
    # def jwt_parse(response):
    #     return json.loads(response.content)['access_token']
    #
    # @endpoint('POST', 'oauth', attach_headers=False, parse_response_func=APIClient.jwt_parse)
    # def jwt_endpoint(self):
    #     return dict(
    #         auth=(self.credentials.client_id, self.credentials.client_secret),
    #         data={'grant_type': 'client_credentials'}
    #     )
    #
    # @endpoint('POST', 'photo')
    # def set_photo(self, file_content):
    #     """
    #     Set profile photo for application
    #     :param file_content: File content
    #     :return: Response
    #     """
    #     return dict(files={'file': file_content})
