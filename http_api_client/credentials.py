from __future__ import absolute_import

import json

from requests.auth import _basic_auth_str


class Credentials(object):
    """
    Abstract Interface to keep authorization data
    """

    def __init__(self, storage=None, service=None, stored_object=None, **kwargs):
        self.storage = storage
        self.service = service
        self.stored_object = stored_object
        self.extra = kwargs

    @property
    def default_authorization_headers(self):
        raise NotImplementedError()

    @classmethod
    def from_json(cls, json_data, **kwargs):
        """
        Class method create Credential instance base on json serialized data
        :param json_data: json serialized data with token and token information
        :return: Credentials instance
        """
        if isinstance(json_data, str):
            json_data = json.loads(json_data)
        json_data.update(kwargs)
        return cls(**json_data)

    def to_json(self):
        """
        Format credential to json
        :return:
        """

        if self.extra:
            return self.extra
        return {}


class PasswordCredentials(Credentials):
    """
    Password base credentials
    fields:
      username: username
      password: password
    """

    def __init__(self, username, password, **kwargs):
        self.username = username
        self.password = password
        super(PasswordCredentials, self).__init__(**kwargs)

    @property
    def default_authorization_headers(self):
        return {'Authorization': _basic_auth_str(self.username, self.password)}

    def to_json(self):
        """
        Format credential to json
        :return:
        """

        return {
            'username': self.username,
            'password': self.password,
        }


class TokenCredentials(Credentials):
    """
    Auth Token base credentials
    fields:
      token: authorization token
    """

    def __init__(self, access_token, **kwargs):
        self.access_token = access_token
        super(TokenCredentials, self).__init__(**kwargs)

    @property
    def token(self):
        return self.access_token

    @property
    def default_authorization_headers(self):
        return {"Authorization": "Bearer {}".format(self.token)}

    def to_json(self):
        """
        Format credential to json
        :return:
        """

        return {
            'token': self.token,
        }


class ClientSecretCredentials(Credentials):
    """
    Client id nad secret base credentials
    fields:
      client_id: client id - app id
      client_secret: client secret
      jwt_token: jwt token set after exchange by API Client
    """

    def __init__(self, client_id, client_secret, **kwargs):
        self.client_id = client_id
        self.client_secret = client_secret
        self.jwt_token = None
        super(ClientSecretCredentials, self).__init__(**kwargs)

    @property
    def default_authorization_headers(self):
        return {"jwt": "{}".format(self.jwt_token)}

    def to_json(self):
        """
        Format credential to json
        :return:
        """

        return {
            'client_id': self.client_id,
            'client_secret': self.client_secret,
        }
