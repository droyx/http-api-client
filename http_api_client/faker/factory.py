from __future__ import absolute_import

import copy
import json
from collections import defaultdict

from .fields import Field


class MetaOptions(object):
    template = None

    def __init__(self, meta=None):
        self.template = getattr(meta, 'template', {})
        self.fields = {}
        self.extra_args = {}
        self.list_algorithm = 'extend'  # override | extend


class FactoryModelMeta(type):

    def __new__(mcs, name, bases, attrs):

        _meta = MetaOptions(attrs.pop('Meta', None))
        module = attrs.pop('__module__')

        new = super(FactoryModelMeta, mcs).__new__(mcs, name, bases, {'__module__': module})

        if _meta:
            new._meta = _meta

        for field_name, field in attrs.items():
            new.add_to_class(field_name, field)

        for base in bases:
            if hasattr(base, '_meta'):
                for field_name, field in base._meta.fields.items():
                    new.add_to_class(field_name, field)

        return new

    def add_to_class(cls, name, instance):
        if hasattr(instance, 'contribute_to_class'):
            instance.contribute_to_class(cls, name)
            return
        setattr(cls, name, instance)


class ResponseModel(object):

    def __init__(self, fields, template):
        self._fields = fields
        self._template = template
        for field_name, field in fields.items():
            value = field
            if hasattr(field, 'create'):
                value = field.create()
            elif hasattr(field, 'to_python'):
                value = field.to_python(self)
            setattr(self, field_name, value)

    def __str__(self):
        return json.dumps(self.render())

    def render(self):
        raise NotImplementedError()

    def render_fields(self):
        result = {}
        for field_name, field in self._fields.items():
            name = getattr(field, 'attr_name', None) or field_name
            value = getattr(self, field_name)
            if hasattr(value, 'render'):
                value = value.render()
            elif isinstance(value, list):
                value_result = []
                for field_item in value:
                    if hasattr(field_item, 'render'):
                        value_result.append(field_item.render())
                    else:
                        value_result.append(field_item)
                value = value_result
            result[name] = value
        return result


class DictResponseModel(ResponseModel):

    def render(self):
        result = {}
        if self._template:
            result = copy.deepcopy(self._template)
        data = self.render_fields()
        result.update(data)
        return result


class ListResponseModel(ResponseModel):

    def __init__(self, fields, template, list_algorithm):
        self.list_algorithm = list_algorithm
        super(ListResponseModel, self).__init__(fields, template)

    def render(self):
        result = []
        if self.list_algorithm == 'extend' and self._template:
            result = copy.deepcopy(self._template)
        fields_dict = self.render_fields()

        result.extend([field_value for field_value in fields_dict.values()])
        return result


class StringResponseModel(ResponseModel):

    def render(self):
        result = self._template or ''
        fields_dict = defaultdict(lambda: '', **self.render_fields())

        return result.format_map(fields_dict)


class ApiFactory(Field, metaclass=FactoryModelMeta):

    def __init__(self, extra_args=None, multiple=None, **kwargs):
        self.multiple = multiple
        self._extra_args = extra_args
        self._extra_kwargs = kwargs
        super(ApiFactory, self).__init__(**kwargs)

    def get_fields(self, extra_args, **kwargs):
        fields = {}
        for field_name, field in self._meta.fields.items():
            if field_name in kwargs:
                if kwargs[field_name] is not None:
                    fields[field_name] = kwargs[field_name]
            else:
                new_field = copy.deepcopy(field)
                if field_name in extra_args:
                    for attr, value in extra_args[field_name].items():
                        setattr(new_field, attr, value)

                if hasattr(new_field, 'get_fields'):
                    new_field.get_fields(extra_args)
                fields[field_name] = new_field
        return fields

    def create(self, extra_args=None, **kwargs):
        final_extra_args = copy.deepcopy(self._meta.extra_args)
        if self._extra_args:
            final_extra_args.update(self._extra_args)
        if extra_args:
            final_extra_args.update(extra_args)
        extra_kwargs = copy.deepcopy(self._extra_kwargs)
        if kwargs:
            extra_kwargs.update(kwargs)
        fields = self.get_fields(final_extra_args, **extra_kwargs)

        if self.multiple:
            return [self._create(fields) for i in range(self.multiple)]

        return self._create(fields)

    def _create(self, fields):
        return DictResponseModel(fields, self._meta.template)


class ListApiFactory(ApiFactory):

    def _create(self, fields):
        return ListResponseModel(fields, self._meta.template, self._meta.list_algorithm)


class StringFactory(ApiFactory):

    def _create(self, fields):
        return StringResponseModel(fields, self._meta.template)
