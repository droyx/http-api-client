from __future__ import absolute_import

from faker import Faker


class Field(object):
    parent = None
    parent_attr_name = None

    def __init__(self, value=None, attr_name=None, **kwargs):
        self.value = value
        self.attr_name = attr_name

    def to_python(self, response_model):
        if hasattr(self.value, '__call__'):
            return self.value()
        return self.value

    def contribute_to_class(self, cls, name):
        setattr(cls, name, self)
        cls._meta.fields[name] = self
        self.parent = cls
        self.parent_attr_name = name


class FakerField(Field):
    """
    Field used faker library for EN_us
    """

    def __init__(self, *args, **kwargs):
        """
        :param faker_attr: kwargs for faker.Faker
        :param kwargs:
        """
        self.faker_attr = kwargs.pop('faker_attr', {})
        super(FakerField, self).__init__(*args, **kwargs)

    def to_python(self, response_model):
        fake = Faker('EN_us')
        return getattr(fake, self.value)(**self.faker_attr)


class MethodField(Field):

    def __init__(self, method_name=None):
        self.method_name = method_name

    def to_python(self, response_model):
        method_name = self.method_name or self.parent_attr_name
        method = getattr(self.parent, 'get_{}'.format(method_name), None)

        if method is not None:
            return method(self.parent, response_model)

        return None
