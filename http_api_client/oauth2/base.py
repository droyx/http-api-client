from __future__ import absolute_import

import logging

from oauthlib.oauth2 import WebApplicationClient, BackendApplicationClient
from requests.exceptions import RequestException

from .exceptions import RefreshCredentialsError
from .request import Oauth2Request


logger = logging.getLogger(__name__)


class Oauth2Base(object):
    """
    Oauth2 client base class
    This class provide all methods to made oauth2 authorization and save result in storage

    """

    def __init__(self, **kwargs):
        for attr, val in kwargs.items():
            setattr(self, attr, val)

    def refresh_token(self, credentials):
        raise NotImplementedError()


class Oauth2WebClient(Oauth2Base):

    authorization_url = ''  # web client oauth2 1st step url
    token_url = ''  # web client oauth2 2nd step url to exchange url
    refresh_token_url = ''  # refresh url
    redirect_url = ''  # redirect_url
    authorization_response = None  # url to redirect after success token exchange

    def __init__(self, client_id, client_secret=None, scope=None, include_client_id=True, **kwargs):
        self.client_id = client_id
        self.client_secret = client_secret
        self.scope = scope or []
        self.include_client_id = include_client_id

        super(Oauth2WebClient, self).__init__(**kwargs)

        self.helper = WebApplicationClient(client_id=client_id, redirect_url=self.redirect_url, scope=self.scope)

    def get_authorization_url(self):
        """
        Prepare 1st step authorization url
        :return: url
        """

        authorization_params = getattr(self, 'authorization_params', {})
        url, header, data = self.helper.prepare_authorization_request(self.authorization_url, scope=self.scope,
                                                                      **authorization_params)
        return url

    def exchange_code(self, code, state=None):
        """
        Exchange code for token in 2nd step of oauth2 code flow
        :param code:
        :return: Credential response
        """
        # TODO validate code and state

        url, headers, data = self.helper.prepare_token_request(self.token_url,
                                                               client_secret=self.client_secret,
                                                               code=code,
                                                               scope=self.scope)
        response = Oauth2Request().post(url, headers=headers, data=data)

        return response

    def refresh_token(self, credentials):
        """
        Refreshing token
        :param credentials: credentials_class instance
        :return:
        """
        url, headers, data = self.helper.prepare_refresh_token_request(self.refresh_token_url,
                                                                       client_secret=self.client_secret,
                                                                       client_id=self.client_id,
                                                                       refresh_token=credentials.refresh_token,
                                                                       scope=self.scope,
                                                                       redirect_uri=self.redirect_url)

        try:
            response = Oauth2Request().post(url, headers=headers, data=data)

        except RequestException as e:
            logger.exception('{}.refresh_token error: {}'.format(self.__class__.__name__, repr(e.args[0])),
                             extra={'request': getattr(e, 'request', None), 'response': getattr(e, 'response', None)})
            raise RefreshCredentialsError('{}.refresh_token error: {}'.format(self.__class__.__name__, repr(e.args[0])),
                                          response=e.response, credentials=credentials)

        return response


class Oauth2BackendApplicationClient(Oauth2Base):

    token_url = ''  # web client oauth2 url to exchange
    refresh_token_url = ''  # refresh url
    redirect_url = ''  # redirect_url
    authorization_response = None  # url to redirect after success token exchange

    def __init__(self, client_id, client_secret=None, scope=None, include_client_id=True, **kwargs):
        self.client_id = client_id
        self.client_secret = client_secret
        self.scope = scope or []
        self.include_client_id = include_client_id

        super(Oauth2BackendApplicationClient, self).__init__(**kwargs)

        self.helper = BackendApplicationClient(client_id=client_id, redirect_url=self.redirect_url, scope=self.scope)

    def exchange(self):
        """
        Authenticate with client id and client secret
        In result receive token
        :return: Credential response
        """

        url, headers, data = self.helper.prepare_token_request(self.token_url,
                                                               client_secret=self.client_secret,
                                                               include_client_id=True,
                                                               scope=self.scope)
        response = Oauth2Request().post(url, headers=headers, data=data)

        return response

    def refresh_token(self, credentials):
        """
        Refreshing token
        If credentials don't contain refresh token, will exchange for new one
        :param credentials: credentials_class instance
        :return:
        """
        if credentials.refresh_token:
            url, headers, data = self.helper.prepare_refresh_token_request(self.refresh_token_url,
                                                                           client_secret=self.client_secret,
                                                                           client_id=self.client_id,
                                                                           refresh_token=credentials.refresh_token,
                                                                           scope=self.scope,
                                                                           redirect_uri=self.redirect_url)

            try:
                response = Oauth2Request().post(url, headers=headers, data=data)

            except RequestException as e:
                logger.exception('{}.refresh_token error: {}'.format(self.__class__.__name__, repr(e.args[0])),
                                 extra={'request': getattr(e, 'request', None), 'response': getattr(e, 'response', None)})
                raise RefreshCredentialsError('{}.refresh_token error: {}'.format(
                    self.__class__.__name__, repr(e.args[0])), response=e.response, credentials=credentials)
        else:
            response = self.exchange()

        return response
