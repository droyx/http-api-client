from __future__ import absolute_import

import datetime

from ..credentials import TokenCredentials


class Oauth2Credentials(TokenCredentials):
    """
    Interface to keep oauth2 authorization token
    """
    DATETIME_FORMAT = "%Y-%m-%dT%H:%M:%SZ"
    expire_date_field = 'token_expiry'
    expire_time_delta = 'expires_in'

    def __init__(self, access_token, refresh_token=None, token_type=None, scope=None,
                 **kwargs):
        self.refresh_token = refresh_token  # Optional
        self.token_type = token_type
        self.scope = scope
        super(Oauth2Credentials, self).__init__(access_token, **kwargs)
        self.expires_at = None
        self.set_expires_at()

    @property
    def token(self):
        """
        Return valid access token
        :return:
        """
        if not self.is_valid():
            self.refresh()
        return self.access_token

    def set_expires_at(self):
        """
        Sets local expire_at value and if only time delta is passed, calculate and save expire date to extra kwargs
        :return:
        """
        if self.expire_date_field in self.extra:
            self.expires_at = datetime.datetime.strptime(self.extra[self.expire_date_field], self.DATETIME_FORMAT)
        elif self.expire_time_delta in self.extra:
            self.expires_at = self.calculate_expire_at(self.extra[self.expire_time_delta])
            self.extra[self.expire_date_field] = self.expires_at.strftime(self.DATETIME_FORMAT)

    def to_json(self):
        """
        Format credential to json
        :return:
        """
        json_data = {
            "access_token": self.token,
            "token_type": self.token_type,
            "scope": self.scope,
            "refresh_token": self.refresh_token,
        }
        if self.extra:
            json_data.update(self.extra)
        return json_data

    def refresh(self):
        """
        Refreshing instance data
        :return:
        """
        if not self.service:
            return
        new_token = self.service.refresh_token(self)
        self.refresh_token = new_token.pop('refresh_token', self.refresh_token)
        self.token_type = new_token.pop('token_type', self.token_type)
        self.scope = new_token.pop('scope', self.scope)
        self.access_token = new_token.pop('access_token', self.access_token)
        self.extra = new_token
        self.expires_at = None
        self.set_expires_at()
        if self.storage:
            self.storage.update(self, self.stored_object)

    def calculate_expire_at(self, expire_in):
        """
        function use time now to set expiration datetime
        :param expire_in: expire in seconds
        :return:
        """
        now = datetime.datetime.utcnow()
        return now + datetime.timedelta(seconds=expire_in)

    def is_valid(self):
        """
        Checks if token not expires
        :return: bool
        """
        now = datetime.datetime.utcnow()
        if self.expires_at is None:
            return True
        return self.expires_at >= now + datetime.timedelta(seconds=60)
