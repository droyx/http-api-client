from __future__ import absolute_import

from requests.exceptions import RequestException


class RefreshCredentialsError(RequestException):
    def __init__(self, *args, **kwargs):
        self.credentials = kwargs.pop('credentials', None)
        self.storage_instance = kwargs.pop('storage_instance', None)
        super(RefreshCredentialsError, self).__init__(*args, **kwargs)


class Oauth2StoredObjectNotExists(Exception):
    pass


class Oauth2StoredObjectDuplicated(Exception):

    def __init__(self, credential_details=None, *args, **kwargs):
        self.credential_details = credential_details
        self.message = 'Token for {email} was already registered for another user'.format(**credential_details)
        super(Oauth2StoredObjectDuplicated, self).__init__(*args, **kwargs)
