from __future__ import absolute_import

import logging

import requests
from requests.exceptions import RequestException


logger = logging.getLogger(__name__)


class Oauth2Request(requests.Session):
    """
    Helper request class to made request in most popular oauth2 flow implementations
    """

    def request(self, *args, **kwargs):
        """
        Make request and parse response
        :param kwargs:
        :return:
        """
        kwargs['headers'] = self.prepare_headers(kwargs.get('headers', {}))
        response = super(Oauth2Request, self).request(*args, **kwargs)

        # We treat HTTP >= 300 as error, response with tokens should not make redirect
        if response.status_code >= 300:
            raise RequestException(repr(response.content)[:250], response=response)

        return response.json()

    def prepare_headers(self, headers):
        headers = headers or {}
        headers.update({'Content-Type': 'application/x-www-form-urlencoded'})
        return headers
