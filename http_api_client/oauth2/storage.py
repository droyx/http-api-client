from ..storage import StorageBase


class Oauth2StorageBase(StorageBase):
    """
    Base class to get and save credentials in concrete storage
    """

    def get_stored(self, authorization_service=None, **kwargs):
        """
        Get object from storage and convert to credential instance
        :param authorization_service: Authorization service
        :param kwargs:
        :return: (<credential_class>, <object>) tuple with  instance of credential_class and storage object
        """
        return None, None
