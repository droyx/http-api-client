class StorageBase(object):
    """
    Base class to get and save credentials in concrete storage
    """

    def __init__(self, credentials_class, **kwargs):
        self.credentials_class = credentials_class
        for attr, val in kwargs.items():
            setattr(self, attr, val)

    def save(self, credentials, *args, **kwargs):
        """
        Save credential instance in storage
        :param credentials:
        :param kwargs:
        :return:
        """
        raise NotImplementedError()

    def update(self, credentials, stored_object):
        raise NotImplementedError()

    def get_stored(self, **kwargs):
        """
        Get object from storage and convert to credential instance
        :param kwargs:
        :return: (<credential_class>, <object>) tuple with  instance of credential_class and storage object
        """
        return None, None

    def reset_additional_data(self, **kwargs):
        """
        Resets any additional data after restarting credential data
        :return:
        """
        pass
