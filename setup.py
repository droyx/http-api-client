from distutils.core import setup

setup(name='http_api_client',
      version='0.1',
      description='Base lib to create API client with oauth2',
      author='Patryk Kisielewski',
      author_email='droyx2@gmail.com',
      install_requires=['oauthlib', 'requests'],
      packages=['http_api_client']
      )
